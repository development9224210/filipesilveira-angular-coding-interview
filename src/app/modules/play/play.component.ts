import { Component, OnInit } from '@angular/core';
import { Subscription, of, throwError } from 'rxjs';
import { catchError, switchMap, tap } from 'rxjs/operators';

import { ActivatedRoute } from '@angular/router';
import { QuestionModel } from '../../core/state/question.model';
import { QuestionsService } from '../../core/services/questions.service';

@Component({
  selector: 'app-play',
  templateUrl: './play.component.html',
  styleUrls: ['./play.component.scss']
})
export class PlayComponent implements OnInit {
  questions$ = this.questionsService.questions$;
  gettingQuestions$ = this.questionsService.gettingQuestions$;
  errorMessage: boolean = false;

  getQuestionsSubscription: Subscription = this.route.queryParams
    .pipe(switchMap(params =>
      this.questionsService.getQuestions({
        type: params.type,
        amount: params.amount,
        difficulty: params.difficulty
      }).pipe(
        catchError((error) => {
          this.errorMessage = true;
          return throwError(error);
        }
      ))
    )).subscribe();


  constructor(
    private readonly route: ActivatedRoute,
    private readonly questionsService: QuestionsService,
  ) { }

  ngOnInit(): void {
  }

  onAnswerClicked(questionId: QuestionModel['_id'], answerSelected: string): void {
    this.questionsService.selectAnswer(questionId, answerSelected);
  }

}
